<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peca extends Model
{
     protected $fillable = ['nome', 'id_fornecedor'];
    
     
    public function fornecedor(){

    	return $this->belongsTo('App\Fornecedor', 'id_fornecedor', 'id');
    }
}
