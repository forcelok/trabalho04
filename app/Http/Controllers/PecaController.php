<?php

namespace App\Http\Controllers;

use App\Peca;
use Illuminate\Http\Request;

class PecaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $atributos = ['peca', 'id_fornecedor'];

    public function index()
    {
       // $qtd = $request->input('qtd');
       // return Peca::with('fornecedor')->paginate();
        
         try{

           return response()->json([Peca::with('fornecedor')->paginate()], 200);

        }catch(\Exception $e){

            return response()->json(["Mensagem"=> $e->getMessage()], 400); }

           // return Peca :: all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
            $this->validate(
            $request,[
                'nome'=>'required|min:3|max:30',
                'id_fornecedor'=>'required|numeric'
            ]
        );
        $peca = new Peca();
        $peca->fill($request->all());
        $peca-> save();

        return $peca;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Peca  $peca
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{

            if( $id > 0 ){
                $peca = Peca::with('fornecedor')->find( $id );
                if( $peca ){
                    return $peca;
                }else{
                    return response()->json( ["mensagem" => "Registro nao encontrado"], 404 );
                }
            }else{
                return response()->json( ["mensagem" => "Parametro invalido"], 400 );
            }


        }catch ( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
    }
}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Peca  $peca
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Peca  $peca
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $peca = Peca::find ($id);
        $peca -> fill ($request->all());
        $peca ->save();
        return $peca;    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Peca  $peca
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $peca = Peca :: find($id);
        $peca -> delete();
        return $peca;
    }
}
