<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fornecedor extends Model
{
	protected $table = 'fornecedores';
    protected $fillable = ['descricao', 'estado', 'uf'];

    public function pecas(){

    	return $this->hasMany('App\Peca', 'id_fornecedor', 'id');
    }
}
