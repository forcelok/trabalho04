<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NomePeca extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pecas', function( Blueprint $table ){
            $table->increments('id');
            $table->string('nome', 40);
            $table->integer('id_fornecedor')->unsigned();
            $table->foreign('id_fornecedor')->references('id')->on('fornecedores');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pecas');
    }
}
