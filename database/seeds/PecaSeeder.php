<?php

use Illuminate\Database\Seeder;
use App\Peca;
class PecaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Peca::create([
       		'id' => 10,
       		'nome' => "Motor",
       		'id_fornecedor'=> "10"
       ]);

        Peca::create([
          'id' => 11,
          'nome' => "Porta",
          'id_fornecedor'=> "11"
       ]);

         Peca::create([
          'id' => 12,
          'nome' => "Capo",
          'id_fornecedor'=> "12"
       ]);

          Peca::create([
          'id' => 13,
          'nome' => "Chave",
          'id_fornecedor'=> "13"
       ]);
           Peca::create([
          'id' => 14,
          'nome' => "Pneu",
          'id_fornecedor'=> "14"
       ]);
    }
}
