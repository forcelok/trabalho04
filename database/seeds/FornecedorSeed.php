<?php

use Illuminate\Database\Seeder;
use App\Fornecedor;
class FornecedorSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Fornecedor::create([
       		'id' => 10,
       		'descricao' => "bianchi",
       		'estado'=> "Parana",
       		'uf'=>"Pr"
       ]);
         Fornecedor::create([
       		'id' => 11,
       		'descricao' => "Ronsoni Peças",
       		'estado'=> "Santa Catarina",
       		'uf'=>"SC"
       ]);
          Fornecedor::create([
       		'id' => 12,
       		'descricao' => "Tabajara",
       		'estado'=> "São Paulo",
       		'uf'=>"SP"
       ]);
           Fornecedor::create([
       		'id' => 13,
       		'descricao' => "Super Peças",
       		'estado'=> "Rio Grando do Sul",
       		'uf'=>"RS"
       ]);
            Fornecedor::create([
       		'id' => 14,
       		'descricao' => "Marquinhos Auto Peças",
       		'estado'=> "Mato Grosso",
       		'uf'=>"MT"
       ]);
    }
}
